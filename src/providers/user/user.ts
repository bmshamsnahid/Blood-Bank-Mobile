import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import {ToastController} from 'ionic-angular';
import {User} from "../../model/user";
import {MyConfig} from "../../model/myConfig";

@Injectable()
export class UserProvider {

  headers: Headers;
  options: RequestOptions;
  myConfig: MyConfig;

  currentUserObj;
  user: User;
  token;

  constructor(public http: Http,
              private toastCtrl: ToastController) {

    this.myConfig = new MyConfig();
    this.headers = new Headers();

    this.headers.append("Content-Type", "application/json");

    try {
      this.currentUserObj = JSON.parse(localStorage.getItem('currentUserObj'));
    } catch (e) {
      this.presentToast('Please sign in to get all the resources.');
    }

    if (typeof this.currentUserObj != 'undefined' && this.currentUserObj != null) {
      this.user = this.currentUserObj.currentUser;
      this.token = this.currentUserObj.token;
      this.headers.append("Authorization", this.token);
    }

    this.options = new RequestOptions({ headers: this.headers});
  }

  getAllUsers() {
    return this.http.get(`${this.myConfig.baseURL}/api/user`, this.options)
      .map((response: Response) => {
        if (response.json().success) {
          return response.json();
        } else if (response.json().message) {
          this.presentToast(response.json().message)
        } else {
          this.presentToast('Fatal server error.');
        }
      });
  }

  createUser(user: User) {
    return this.http.post(`${this.myConfig.baseURL}/api/user`, JSON.stringify(user), this.options)
      .map((response: Response) => {
        console.log(response);
        if (response.json().success) {
          return response.json();
        } else if (response.json().message) {
          this.presentToast(response.json().message)
        } else {
          this.presentToast('Fatal server error.');
        }
      });
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

}
