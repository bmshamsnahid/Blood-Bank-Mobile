import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {User} from "../../model/user";
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import {ToastController} from 'ionic-angular';
import {MyConfig} from "../../model/myConfig";


@Injectable()
export class SignInProvider {

  headers: Headers;
  myConfig: MyConfig;
  currentUser: User;

  constructor(public http: Http,
              private toastCtrl: ToastController) {
    this.myConfig = new MyConfig();
    this.headers = new Headers();
    this.headers.append("Content-Type", "application/json");
  }

  isLoggedIn(): boolean {
    try {
      let currentUserObj = JSON.parse(localStorage.getItem('currentUserObj'));
      let currentUser = currentUserObj.currentUser;

      if (currentUser) {
        this.currentUser = currentUser;
        return true;
      }
    } catch (e) {
      return false;
    }
    return false;
  }

  userSignIn(user) {
    const options = new RequestOptions({ headers: this.headers});

    return this.http.post(`${this.myConfig.baseURL}/api/auth/login`, JSON.stringify(user), options)
      .map((response: Response) => {
        if (response.json().success) {
          localStorage.setItem('email', user.email);
          localStorage.setItem('password', user.password);
          let userObj: any = {};
          userObj.currentUser = response.json().data;
          userObj.token = response.json().token;
          localStorage.setItem('currentUserObj', JSON.stringify(userObj));
          return response.json();
        } else if (response.json().message) {
          this.presentToast(response.json().message);
        } else {
          this.presentToast('Fatal Server Error');
        }
      });
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

}
