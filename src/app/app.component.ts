import { Component } from '@angular/core';
import {Platform, ToastController} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
import {SignUpPage} from "../pages/sign-up/sign-up";
import {SignInPage} from "../pages/sign-in/sign-in";
import {SignInProvider} from "../providers/sign-in/sign-in";
import {User} from "../model/user";
import {Page} from "ionic-angular/navigation/nav-util";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = SignInPage;
  signInPage: Page = SignInPage;

  constructor(platform: Platform,
              statusBar: StatusBar,
              splashScreen: SplashScreen,
              private signInProvider: SignInProvider,
              private toastCtrl: ToastController) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
    this.initiatePage();
  }

  initiatePage() {
    try {
      if (this.signInProvider.isLoggedIn()) {
        this.rootPage = TabsPage;
        let user: User = new User();
        user.email = localStorage.getItem('email');
        user.password = localStorage.getItem('password');
        this.signInProvider.userSignIn(user).subscribe((response) => {
          if (response.success) {
            this.presentToast('Your entry activity updated.')
          } else {
            this.presentToast('Please login again.');
            this.rootPage = this.signInPage;
          }
        });
      } else {
        this.rootPage = this.signInPage;
      }
    } catch (e) {
      console.log(e);
      this.rootPage = this.signInPage;
    }
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

}
