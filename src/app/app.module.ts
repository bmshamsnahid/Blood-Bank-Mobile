import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {SignInPageModule} from "../pages/sign-in/sign-in.module";
import {SignUpPageModule} from "../pages/sign-up/sign-up.module";
import {HomePageModule} from "../pages/home/home.module";
import {SearchPageModule} from "../pages/search/search.module";
import {SettingsPageModule} from "../pages/settings/settings.module";
import {ViewProfilePageModule} from "../pages/view-profile/view-profile.module";
import {EditProfilePageModule} from "../pages/edit-profile/edit-profile.module";
import { UserProvider } from '../providers/user/user';
import { SignInProvider } from '../providers/sign-in/sign-in';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";

@NgModule({
  declarations: [
    MyApp,
    TabsPage
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    SignInPageModule,
    SignUpPageModule,
    HomePageModule,
    SearchPageModule,
    SettingsPageModule,
    ViewProfilePageModule,
    EditProfilePageModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UserProvider,
    SignInProvider
  ]
})
export class AppModule {}
