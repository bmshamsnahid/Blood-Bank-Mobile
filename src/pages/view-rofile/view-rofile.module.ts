import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewRofilePage } from './view-rofile';

@NgModule({
  declarations: [
    ViewRofilePage,
  ],
  imports: [
    IonicPageModule.forChild(ViewRofilePage),
  ],
})
export class ViewRofilePageModule {}
