import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {UserProvider} from "../../providers/user/user";
import {User} from "../../model/user";

/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  users: User[];

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private userProvider: UserProvider,
              private toastCtrl: ToastController) {
    this.userProvider.getAllUsers()
      .subscribe((response) => {
        if (response.success) {
          this.users = response.data;
          console.log(this.users);
        }
      });
  }

  onClickText(user: User) {

  }

  onClickCall(user: User) {

  }

  onClickEmail(user: User) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }

}
