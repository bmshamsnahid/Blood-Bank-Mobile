import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {UserProvider} from "../../providers/user/user";
import {User} from "../../model/user";
import {SignInProvider} from "../../providers/sign-in/sign-in";
import {Page} from "ionic-angular/navigation/nav-util";
import {TabsPage} from "../tabs/tabs";
import {SignUpPage} from "../sign-up/sign-up";

/**
 * Generated class for the SignInPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sign-in',
  templateUrl: 'sign-in.html',
})
export class SignInPage {

  email: string;
  password: string;

  user: User;

  tabsPage: Page = TabsPage;
  signUpPage: Page = SignUpPage;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private toastCtrl: ToastController,
              private signInProvider: SignInProvider) {
    this.user = new User();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignInPage');
  }

  onClickLogin() {
    if (!this.email) {
      this.presentToast('Invalid or incomplete email.');
    } else if (!this.password) {
      this.presentToast('Invalid or incomplete email.');
    } else {
        this.user.email = this.email;
        this.user.password = this.password;
        this.signInProvider.userSignIn(this.user)
          .subscribe((response) => {
            if (response.success) {
              this.presentToast('Successfully logged in.');
              this.navCtrl.push(this.tabsPage);
            }
          });
    }
  }

  onClickSignup() {
    this.navCtrl.push(this.signUpPage);
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

}
