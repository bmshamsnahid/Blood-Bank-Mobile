import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {User} from "../../model/user";
import {UserProvider} from "../../providers/user/user";
import {Page} from "ionic-angular/navigation/nav-util";
import {SignInPage} from "../sign-in/sign-in";

/**
 * Generated class for the SignUpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sign-up',
  templateUrl: 'sign-up.html',
})
export class SignUpPage {

  user: User;
  name: string;
  number: string;
  email: string;
  password: string;
  location: string;
  bloodGroup: string;

  reTypePassword: string;

  signInPage: Page = SignInPage;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private userProvider: UserProvider,
              private toastCtrl: ToastController) {
    this.user = new User();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignUpPage');
  }

  onClickSignup() {
    if (!this.name) {
      this.presentToast('Invalid or incomplete name.')
    } else if (!this.number) {
      this.presentToast('Invalid or incomplete email.');
    } else if (!this.email) {
      this.presentToast('Invalid or incomplete email.');
    } else if (!this.password) {
      this.presentToast('Invalid or incomplete password.');
    } else if (!this.location) {
      this.presentToast('Invalid or incomplete location.');
    } else if (!this.bloodGroup) {
      this.presentToast('Invalid or incomplete blood group.');
    } else if (!this.reTypePassword) {
      this.presentToast('Type password again.');
    } else if (this.password != this.reTypePassword) {
      this.presentToast('Password does not match');
    } else {
      this.user.name = this.name;
      this.user.email = this.email;
      this.user.number = this.number;
      this.user.password = this.password;
      this.user.location = this.location;
      this.user.bloodGroup = this.bloodGroup;
      this.userProvider.createUser(this.user)
        .subscribe((response) => {
          if (response.success) {
            this.presentToast('Successfully signed up.');
            this.navCtrl.push(SignInPage);
          }
        });
    }
  }

  onClickLogin() {
    this.navCtrl.push(this.signInPage);
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

}
